import { Component, Input } from '@angular/core';
import { ChatRoom } from "../model/chat";

@Component({
  selector: 'app-chat-messages',
  templateUrl: './chat-messages.component.html',
  styleUrls: ['./chat-messages.component.css']
})
export class ChatMessagesComponent {

  @Input() room:ChatRoom;

}
