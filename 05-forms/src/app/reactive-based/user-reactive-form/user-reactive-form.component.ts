import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { User } from '../../model/user';

@Component({
  selector: 'app-user-reactive-form',
  templateUrl: './user-reactive-form.component.html',
  styleUrls: ['./user-reactive-form.component.css']
})
export class UserReactiveFormComponent implements OnInit {

  @Input() user: User;

  @Output() userSubmit = new EventEmitter<User>();

  userForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    // TODO 2.1 - vytvořte definici formuláře
    this.userForm = null;

    // TODO 2.3 - předejte data do formuláře

    // TODO 2.5 - sledujte změny dat ve formuláři
  }

  onSubmit(form) {
    console.log('onSubmit', form);
    if (form.valid) {
      this.userSubmit.emit(form.value);
    }
  }

}
