import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserTemplateBasedFormComponent } from './user-template-based-form/user-template-based-form.component';
import { SharedModule } from "../shared/shared.module";
import { FormModelErrorsComponent } from './form-model-errors/form-model-errors.component';

@NgModule({

  // TODO 1 - importujte modul formulářů
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    UserTemplateBasedFormComponent,
    FormModelErrorsComponent
  ],
  exports: [
    UserTemplateBasedFormComponent
   ]
})
export class TemplateBasedModule {
}
