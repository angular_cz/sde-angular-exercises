import { Component } from '@angular/core';
import { SavedCalculationsService } from '../saved-calculations.service';

@Component({
  selector: 'app-container',
  templateUrl: 'container.component.html',
  providers: [SavedCalculationsService]
})
export class ContainerComponent  {}
