import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Order } from '../model/order';

@Component({
  selector: 'app-orders-list',
  templateUrl: 'orders-list.component.html'
})
export class OrdersListComponent {

  @Input() orders: Order[];
  @Input() selectedOrder: Order;

  @Output() onSelect = new EventEmitter<Order>();

  select(order: Order) {
    this.onSelect.emit(order);
  }
}
